# nfttoken

## 介绍

    nft token 提供有关erc721协议的nft token合约以及整套golang访问接口。

    golang接口支持所有符合erc721协议智能合约的访问。

## 代码结构

```
nfttoken
│  README.md -- 说明文档
│
├─contract
│  │  nft-token-erc721.sol -- ERC721合约的一个实现例子
│  │
│  ├─base
│  │      erc721-enumerable.sol
│  │      erc721-metadata.sol
│  │      erc721-token-receiver.sol
│  │      erc721.sol
│  │
│  ├─ownership
│  │      ownable.sol
│  │
│  └─utils
│          address-utils.sol
│          erc165.sol
│          supports-interface.sol
│
├─doc
│      NFT智能合约.md
│      接口文档.md
│
└─src
    │  go.mod
    │  go.sum
    │  main.go
    │
    ├─account - 以太坊账户管理
    │      create.go
    │      create_test.go
    │
    ├─common - 项目基础库
    │      conf.go
    │      error.go
    │      log.go
    │
    ├─conf - 配置文件目录
    │      config.conf
    │
    ├─contract - abigen生成的通用访问erc721类型合约的golang文件
    │      Token.go
    │
    ├─event - 合约事件通知
    │      event.go
    │
    ├─handler - 提供外部接口实现
    │      approval.go
    │      createAccount.go
    │      init.go
    │      mint.go
    │      notify.go
    │      query.go
    │      transfer.go
    │      uploadToIpfs.go
    │
    └─trade - 调用智能合约接口
            client.go
            trade.go
```

---

## 部署说明

1. 部署合约（非必须）

    如果需要部署一个ERC721智能合约，可参考`contract`目录下已实现的合约，是否满足实际需求。

    可参考`doc/NFT智能合约.md`文档中更详细介绍了智能合约的编译、部署操作。

2. `src/conf/config.conf`配置文件说明

    ```js
    {
        "runEnv": "test", // 运行环境说明
        "serverAddr": ":8125", // 程序运行ip:端口
        "logLevel": 0, // 日志级别 0 debug 1 info 2 warn 3 error
        "signRandomKey": "", // 加密串
        "secretWord": "", // 钱包账户密码（重要！）；当运行环境为prod时，程序启动后会清楚掉该配置，再下一次启动时需要重新输入，请务必谨慎保管
        "walletPath": "./wallet", // 钱包账户keystore文件保存路径
        "supportChains": { // 支持链类型，可配置多个eth兼容链
            "eth": { // 名称需要唯一，如eth,bsc....
                "chainId": 0, // 该链具体chain id
                "nodePath": "", // 使用http访问该链的路径
                "nodeWssPath": "", // 该链wss地址
                "watchAddress": [ // 监听erc721合约上事件，可填入多个erc721合约地址

                ]
            },
            "bsc": {
                "chainId": 56,
                "nodePath": "",
                "nodeWssPath": "",
                "watchAddress": [

                ]
            }
        },
        "notifyHttpPath": "", // 事件通知请求http根路径，若未配置则不发送通知
        "ipfsNodePath": "" // ipfs节点路径，若未配置则不能上传文件到ipfs
    }
    ```

---

## 其他说明

1. 申请使用第三方eth接口

    部署本地ETH节点需要进行同步操作，而同步操作又比较耗时费力，所以对于没有本地ETH节点的情况下，我们可以通过`infura.io`访问ETH。

    访问`infura.io`网站申请项目账号，并得到https和wss地址。也可使用其他可用地址。

    其他兼容ETH链的访问接口，请相应参考配置。

2.  本地安装ipfs

    - ubuntu 安装 ipfs : sudo snap install ipfs
    - 在snap/ipfs目录下执行初始化

     ```shell
        ipfs init
        > generating ED25519 keypair...done
        peer identity: 12D3KooWMFGDdcwNucoxjBScdwGZsvZJPPDqLdEiZPU7gJGuEYdC
        initializing IPFS node at /root/snap/ipfs/common
        to get started, enter:
        >
        >ipfs cat /ipfs/QmQPeNsJPyVWPFDVHb77w8G42Fvo15z4bG2X8D2GhfbSXc/readme
    ```

    - 执行上个步骤中提示：ipfs cat /ipfs/QmQPeNsJPyVWPFDVHb77w8G42Fvo15z4bG2X8D2GhfbSXc/readme

    - 执行ipfs daemon启动ipfs程序

---

## 打赏

开源不易，如有帮助，可请喝杯咖啡。币圈链圈不差钱 ^ _ ^

![reward](./doc/pic/reward.jpg)
