package common

type NftError struct {
	ErrorNo  int    // 错误码
	ErrorMsg string // 错误描述
}

var errMsg map[int]string

func init() {
	errMsg = make(map[int]string)
	errMsg[200] = "ok"
	errMsg[400] = "input error"
	errMsg[401] = "auth failed"
	errMsg[402] = "get url file failed"
	errMsg[403] = "input tokenid not correct"
	errMsg[500] = "visit chain node failed"
	errMsg[501] = "can not visit input caller address"
	errMsg[502] = "trade to blockchain failed"
	errMsg[503] = "not set ipfs node path"
	errMsg[504] = "upload file to ipfs failed"
	errMsg[505] = "unsupported chain"
	errMsg[506] = "failed to visit input contract"
	errMsg[507] = "can not create new account"
}

func NewError(code int) NftError {
	v, ok := errMsg[code]
	if ok {
		return NftError{ErrorNo: code, ErrorMsg: v}
	} else {
		return NftError{ErrorNo: code, ErrorMsg: "Unknown error"}
	}
}

func Valid(err NftError) bool {
	return err.ErrorNo == 200
}
