package handler

import (
	"crypto"
	_ "crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"io/ioutil"
	"net/http"

	"gitcode.net/togolife/nfttoken/common"
	"gitcode.net/togolife/nfttoken/trade"
)

type MintInput struct {
	ReqChain     string `json:"reqChain"`
	ContractAddr string `json:"contractAddr"`
	CallAddr     string `json:"callAddr"`
	ToAddr       string `json:"toAddr"`
	TokenId      string `json:"tokenId"`
	TokenUri     string `json:"tokenUri"`
	Sign         string `json:"sign"`
}

type MintOutput struct {
	RetCode int    `json:"retCode"`
	RetMsg  string `json:"retMsg"`
	TxHash  string `json:"txHash,omitempty"`
}

func MintRet(w http.ResponseWriter, out *MintOutput) {
	v, err := json.Marshal(out)
	if err != nil {
		return
	}
	w.Write(v)
}

func setMintOutput(out *MintOutput, err common.NftError) {
	out.RetCode = err.ErrorNo
	out.RetMsg = err.ErrorMsg
}

func (input *MintInput) checkSign() bool {
	s := "callAddr=" + input.CallAddr
	s += "&contractAddr=" + input.ContractAddr
	s += "&reqChain=" + input.ReqChain
	s += "&sign=" + hdConf.SignRandomKey
	s += "&toAddr=" + input.ToAddr
	s += "&tokenId=" + input.TokenId
	s += "&tokenUri=" + input.TokenUri
	m := crypto.SHA256.New()
	m.Write([]byte(s))
	v := hex.EncodeToString(m.Sum(nil))
	return v == input.Sign
}

func MintIf(w http.ResponseWriter, req *http.Request) {
	input := MintInput{}
	output := MintOutput{}
	defer MintRet(w, &output)
	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		hdLog.LogE("Read mint http request body failed! [%v]", err)
		setMintOutput(&output, common.NewError(400))
		return
	}
	err = json.Unmarshal(body, &input)
	if err != nil {
		hdLog.LogE("Decode mint http request body failed! [%v]", err)
		setMintOutput(&output, common.NewError(400))
		return
	}
	if !input.checkSign() {
		hdLog.LogE("Check input sign failed!")
		setMintOutput(&output, common.NewError(401))
		return
	}
	tx, nftErr := trade.Mint(input.ReqChain, input.ContractAddr, input.CallAddr, input.ToAddr, input.TokenId, input.TokenUri)
	if !common.Valid(nftErr) {
		hdLog.LogE("Mint failed [%v]", nftErr.ErrorMsg)
		setMintOutput(&output, nftErr)
		return
	}
	setMintOutput(&output, common.NewError(200))
	output.TxHash = tx
	return
}

type BurnInput struct {
	ReqChain     string `json:"reqChain"`
	ContractAddr string `json:"contractAddr"`
	CallAddr     string `json:"callAddr"`
	TokenId      string `json:"tokenId"`
	Sign         string `json:"sign"`
}

type BurnOutput struct {
	RetCode int    `json:"retCode"`
	RetMsg  string `json:"retMsg"`
	TxHash  string `json:"txHash,omitempty"`
}

func BurnRet(w http.ResponseWriter, out *BurnOutput) {
	v, err := json.Marshal(out)
	if err != nil {
		return
	}
	w.Write(v)
}

func setBurnOutput(out *BurnOutput, err common.NftError) {
	out.RetCode = err.ErrorNo
	out.RetMsg = err.ErrorMsg
}

func (input *BurnInput) checkSign() bool {
	s := "callAddr=" + input.CallAddr
	s += "&contractAddr=" + input.ContractAddr
	s += "&reqChain=" + input.ReqChain
	s += "&sign=" + hdConf.SignRandomKey
	s += "&tokenId=" + input.TokenId
	m := crypto.SHA256.New()
	m.Write([]byte(s))
	v := hex.EncodeToString(m.Sum(nil))
	return v == input.Sign
}

func BurnIf(w http.ResponseWriter, req *http.Request) {
	input := BurnInput{}
	output := BurnOutput{}
	defer BurnRet(w, &output)
	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		hdLog.LogE("Read burn http request body failed! [%v]", err)
		setBurnOutput(&output, common.NewError(400))
		return
	}
	err = json.Unmarshal(body, &input)
	if err != nil {
		hdLog.LogE("Decode burn http request body failed! [%v]", err)
		setBurnOutput(&output, common.NewError(400))
		return
	}
	if !input.checkSign() {
		hdLog.LogE("Check input sign failed!")
		setBurnOutput(&output, common.NewError(401))
		return
	}
	tx, nftErr := trade.Burn(input.ReqChain, input.ContractAddr, input.CallAddr, input.TokenId)
	if !common.Valid(nftErr) {
		hdLog.LogE("Burn failed [%v]", nftErr.ErrorMsg)
		setBurnOutput(&output, nftErr)
		return
	}
	setBurnOutput(&output, common.NewError(200))
	output.TxHash = tx
	return
}
